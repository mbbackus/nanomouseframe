$fn=64;

cells = 2.5; //5x5
postDiameter = 7.9;
border = 20;

module starterSquare()
{
  difference()
  {
    translate([-border,-border])
      square([180*cells+border,180*cells+border]);
    for(i=[0:5])
    {
      for(j=[0:5])
      {
        translate([180*i,180*j])
          circle(d=postDiameter);
      }
    }
    translate([2.5*180,180])
    rotate(90)
      tab();
  }
}

module tab()
{
  translate([0,30])
  {
    difference()
    {
      translate([90,0])
        scale([2,1])
          circle(r=50);
      translate([-10,-30])
        square([200,30]);
    }
    hull()
    {
      circle(r=10);
      translate([180,0])
        circle(r=10);

    }
  }
  translate([0,10])
  difference()
  {
    square([180,20]);
    circle(r=10);
    translate([180,0])
      circle(r=10);
  }
  translate([10,0])
    square([160,10]);
}

module puzzlePiece()
{
  starterSquare();
  translate([180,2.5*180])
  tab();
}

puzzlePiece();