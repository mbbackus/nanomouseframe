$fn = 128;

innerDiameter = 2.225*25.4;
outerDiameter = 2.645*25.4;

stretchFactor = 1.03; //Percentate of wheel diameter

servoPostDiameter = 6;
screwHoleDiameter = 3.5;

diameterORing = (outerDiameter-innerDiameter)/2;
echo("Diameter of the O-Ring: ",diameterORing,"mm");

zHub = 5.75;
zWheel = diameterORing+2;

spokeWidth = 3;
fillet = 2;

wheelDiameter = (innerDiameter+outerDiameter)/2;

difference()
{
    linear_extrude(height=zWheel)
    {
        offset(r=-fillet)
        {
            difference()
            {
                circle(d=wheelDiameter*stretchFactor+fillet*2-diameterORing*tan(22.5));
                circle(d=innerDiameter*stretchFactor-6-fillet*2);
            }
            circle(d=zHub+6+fillet*2);
            for(i = [0:2])
            {
                rotate(i*120)
                    translate([-(spokeWidth+2*fillet)/2,0])
                        square([spokeWidth+2*fillet,(innerDiameter*stretchFactor)/2]);
            }
        }
    }
    
	translate([0,0,diameterORing/2+1])
		rotate_extrude(twist=360)
			translate([(wheelDiameter*stretchFactor)/2,0,0])
                rotate([0,0,22.5])
                    circle(d=diameterORing/cos(22.5),$fn=8);

	translate([0,0,2])
		cylinder(d=servoPostDiameter,h=zWheel-2+.01);

	translate([0,0,-.01])
		cylinder(d=screwHoleDiameter,h=2.02);

	translate([0,0,zHub])
		cylinder(d1=innerDiameter*stretchFactor-2-(zWheel-zHub)*2,d2=innerDiameter*stretchFactor-2,h=zWheel-zHub+.01);
}