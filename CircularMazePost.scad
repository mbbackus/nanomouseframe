$fn=64;

materialWidth = 2.4;
gripLength = 4;
taper = .55;
height = 50; //Should be 50
coreRadius = 1;

postDiameter = 8.05;
postLength = 5;
miter = .5;

/*
nailDiameter = 0;
nailHeadDiameter = 0;
nailHeadLength = 0;
nailLength = 0;
*/

nailDiameter = 2.6;
nailHeadDiameter = 3.9;
nailHeadLength = 2.5;
nailLength = 51;

difference()
{
	union()
	{
		cylinder(r=coreRadius+materialWidth/2+gripLength,h=height);
		
		translate([-(materialWidth + 2)/2,-(materialWidth + 2)/2,0])
			cube([materialWidth + 2,materialWidth + 2,height]);
		translate([0,0,height])
		{
			cylinder(d=postDiameter,h=postLength-miter);
			translate([0,0,postLength-miter])
				cylinder(d1=postDiameter,d2=postDiameter-.5,h=miter);	
		}
	}

	for(i = [0:3])
	{
		rotate(i*90)
		{
			linear_extrude(height = height+.5)
			{
					polygon(points=[[materialWidth/2+coreRadius,materialWidth/2],[materialWidth/2+coreRadius+gripLength+.1,materialWidth/2-taper],[materialWidth/2+coreRadius+gripLength+.1,-materialWidth/2+taper],[materialWidth/2+coreRadius,-materialWidth/2]]);
			}
		}
	}
	
	translate([0,0,height+postLength-nailLength])
		cylinder(d=nailDiameter,h=nailLength);
	translate([0,0,height+postLength-nailHeadLength])
		cylinder(d=nailHeadDiameter,h=nailHeadLength);
}