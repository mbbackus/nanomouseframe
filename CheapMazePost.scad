//1.5 grips styrene tightly
//1.7 snugly grips styrene
materialWidth = 1.7;
gripLength = 4;
taper = .1;
height = 50;

union() {	
	for(i = [0:3])
	{
		rotate(i*90)
		{
			polyhedron
			(
				points = [
					[materialWidth/2 + 1,materialWidth/2,-height/2],
					[materialWidth/2 + 1 + gripLength,materialWidth/2 - taper,-height/2],
					[materialWidth/2 - taper,materialWidth/2 + 1 + gripLength,-height/2],
					[materialWidth/2,materialWidth/2 + 1,-height/2],
					[materialWidth/2 + 1,materialWidth/2,height/2],
					[materialWidth/2 + 1 + gripLength,materialWidth/2 - taper,height/2],
					[materialWidth/2 - taper,materialWidth/2 + 1 + gripLength,height/2],
					[materialWidth/2,materialWidth/2 + 1,height/2]
				],
				faces = [
					[0,3,2,1],
					[4,5,6,7],
					[0,1,5,4],
					[1,2,6,5],
					[2,3,7,6],
					[0,4,7,3]
				]
			);
		}
	}

	cube(size = [materialWidth + 2,materialWidth + 2,height], center = true);
}