This program is for use with OpenSCAD to generate the necessary files to print a frame for a Nano Mouse. For more information about what a Nano Mouse is, visit: [https://sites.google.com/site/mbbackus/robotics/nanomouse](https://sites.google.com/site/mbbackus/robotics/nanomouse)

The first Nano Mouse frame was created using SketchUp. My 8th attempt proved functional and was used during the first year of this project, and is the frame used on the mouse seen in the promotional and the majority of the instructional videos. It is referred to hereafter as Version 1.8. After a year of feedback I decided to improve on the design in many ways and to generate the file using OpenSCAD. Thus, this repository begins with version 2.0a1 and uses the following conventions:

* The first number will indicate major revisions that will affect compatibility with the rest of the robot's design, circuitry, and/or programming.
* The second number will indicate minor revisions, improvement, and fixes that will not affect compatibility with the rest of the robot's design, circuitry, and/or programming.
* a refers to alpha testing which will be done by me.
* b refers to beta testing which will be carried out by Twindly Bridge students

How do I use this file?

* Open NanoMouseFrame with OpenSCAD.
* Press F5 to compile the model in color (color is used to refer to different parts of the frame)
* Press F6 to compile and render the model using CGAL which is necessary prior to generating an STL file.
* File >> Export >> Export as STL... to generate an STL file for use with 3D printing software

This repo was created and is maintained by Michael Backus. If you are interested in contributing, please contact me using my *Contact Me* form at: [https://sites.google.com/site/mbbackus/contact-me](https://sites.google.com/site/mbbackus/contact-me)