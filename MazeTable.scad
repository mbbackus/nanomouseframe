$fn = 64;
wallLengthmm = 180;
wallLengthin = 180/25.4;
border = 1;

echo("Maze dimensions: ",wallLengthin*5+2*border," by ",wallLengthin*5+2*border," square inches.");

module maze()
{
    difference()
    {
        square([wallLengthin*5+2*border,wallLengthin*5+2*border]);
    
        translate([border,border])
        {
            for(i = [0:5])
            {
                for(j = [0:5])
                {
                    translate([i*wallLengthin,j*wallLengthin])
                        circle(d=.25);
                }
            }
        }
    }
}

maze();

translate([39,0,0])
    maze();